package com.elchupacabra.elchupapatrol;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.elchupacabra.elchupapatrol.ground.generator.GroundGenerator;
import com.elchupacabra.elchupapatrol.pawns.testPawn;
import com.elchupacabra.elchupapatrol.pawns.car.Car;
import com.elchupacabra.elchupapatrol.pawns.car.CarBuilder;
import com.elchupacabra.elchupapatrol.physics.CollisionListener;
import com.elchupacabra.elchupapatrol.physics.Physics;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
*	@author Łukasz Piotrowski
*
*	Core Game class
*
*/

public final class GameCore {
	private AssetsManager assetsManager;
	private OrthographicCamera camera;
	private SpriteBatch spriteBatch;
	private PolygonSpriteBatch polyBatch;
	private Physics physics;
	private GroundGenerator groundGenerator;
	private CarBuilder carBuilder;
	private Car car;
	private CollisionListener collisionListener;
	
	private Settings settings;
	

	public GameCore () {
		settings = Settings.getInstance();
		physics = Physics.getInstance();
		assetsManager = AssetsManager.getInstance();
		camera = Camera.getInstance().camera;
		spriteBatch = new SpriteBatch();
		polyBatch = new PolygonSpriteBatch();
		groundGenerator = GroundGenerator.getInstance();
		carBuilder = new CarBuilder();
		car = carBuilder.assemblyCar();
		physics.world.setContactListener( new CollisionListener() );
	}
	
	/**
	*	Core update method - for simplicity sake, and
	*	to keep all non-render code outside of render method.
	*/
	public void update () {
		AutoRenderer.getInstance().update();
		camera.update();
		physics.update();
		car.update();
		groundGenerator.update();
	}
	
	/**
	*	Core render method. All opengl-based operations
	*	should be done here.
	*/
	public void render () {
		Gdx.gl.glClearColor( 147.0f/255.0f, 228.0f/255.0f, 245.0f/255.0f, 1 );
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		
		AutoRenderer.getInstance().render( spriteBatch, polyBatch );
		
		physics.debugRender();
	}
}
