package com.elchupacabra.elchupapatrol.entities;

import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;


/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Class used in creation of Polygonal Sprites with
 * kinematic Body Type Fixtures
 * 
 * 
 */
public abstract class KinematicPolyEntity extends PolyEntity {

	/**
	 * Creates KinematicPolyEntity with given origin point, set of verticies, 
	 * texture name and type of texture wrap.
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 * @param wrap Texture wrap type : TextureWrap.Repeat or TextureWrap.MirroredRepeat
	 */
	protected KinematicPolyEntity( float originX, float originY, float[] verts,
			String textureName, TextureWrap wrap, 
			int textureOffsetX, int textureOffsetY  ) {
		super( originX, originY, verts, textureName, wrap, 
				textureOffsetX, textureOffsetY  );
		setFixture( originX, originY, verts );
	}
	
	/**
	 * Creates KinematicPolyEntity with given origin point, set of verticies, 
	 * texture name and repeated texture wrap
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 */
	protected KinematicPolyEntity ( float originX, float originY, float[] verts,
			String textureName, int textureOffsetX, int textureOffsetY  ) {
		super( originX, originY, verts, textureName, textureOffsetX, textureOffsetY  );
		setFixture( originX, originY, verts );
	}
	
	/**
	 * Creates fixture for KinematicPolyEntity
	 * @param originX orgin point for fixture (in pixels)
	 * @param originY orgin point for fixture (in pixels)
	 * @param verts fixture's shape verticies (mind the order)
	 */
	private void setFixture ( float originX, float originY, float[] verts ) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.KinematicBody;
		super.setFixture( originX, originY, verts, bodyDef );
	}

}
