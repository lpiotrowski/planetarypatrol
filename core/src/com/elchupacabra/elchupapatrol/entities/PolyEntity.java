package com.elchupacabra.elchupapatrol.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.elchupacabra.elchupapatrol.AssetsManager;
import com.elchupacabra.elchupapatrol.exceptions.NoAssetWithGivenNameException;
import com.elchupacabra.elchupapatrol.physics.Physics;

/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Class used in creation of physics-enabled
 * Polygonal Sprites
 * 
 * 
 */
public abstract class PolyEntity extends Entity {
	/** Polygon with sprite, visual representation of PolyEntity */
	protected PolygonSprite poly;
	
	/**
	 * Creates PolyEntity with given origin point, set of verticies, 
	 * texture name and type of texture wrap.
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 * @param wrap Texture wrap type : TextureWrap.Repeat or TextureWrap.MirroredRepeat
	 */
	protected PolyEntity ( float originX, float originY, float[] verts, 
			String textureName, TextureWrap wrap, 
			int textureOffsetX, int textureOffsetY ) {
		setupPolyEntity( originX, originY, verts, textureName, wrap,
				textureOffsetX, textureOffsetY );
	}
	
	/**
	 * Creates PolyEntity with given origin point, set of verticies, 
	 * texture name and repeated texture wrap
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 */
	protected PolyEntity ( float originX, float originY, float[] verts, 
			String textureName, int textureOffsetX, int textureOffsetY ) {
		setupPolyEntity( originX, originY, verts, textureName, TextureWrap.Repeat,
				textureOffsetX, textureOffsetY );
	}
	
	/**
	 * Sets up PolyEntity with given origin point, set of verticies, 
	 * texture name and type of texture wrap.
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 * @param wrap Texture wrap type : TextureWrap.Repeat or TextureWrap.MirroredRepeat
	 */
	private void setupPolyEntity ( float originX, float originY, float[] verts, 
			String textureName, TextureWrap wrap, 
			int textureOffsetX, int textureOffsetY ){
		physics = Physics.getInstance();
		setPolygon( originX, originY, verts, textureName, wrap, 
				textureOffsetX, textureOffsetY );
	}
	
	/**
	 * Creates PolygonSprite for PolyEntity for rendering
	 * @param originX origin point in world (in pixels)
	 * @param originY origin point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 * @param wrap Texture wrap type : TextureWrap.Repeat or TextureWrap.MirroredRepeat
	 */
	private void setPolygon ( float originX, float originY, float[] verts, 
			String textureName, TextureWrap wrap, 
			int textureOffsetX, int textureOffsetY ) {
		short triangles[] = new EarClippingTriangulator()
									.computeTriangles( verts )
									.toArray();
		TextureRegion texReg;
		try {
			texReg = new TextureRegion( AssetsManager.getInstance().getTexture( textureName ) );
			TextureRegion texReg2 = new TextureRegion();
			texReg.setRegionX( textureOffsetX );
			texReg.setRegionY( textureOffsetY );
			PolygonRegion polyReg = new PolygonRegion( texReg, verts, triangles );
		    poly = new PolygonSprite( polyReg );
		    poly.setPosition( originX, originY );
		} catch (NoAssetWithGivenNameException e) {
			e.getMessage();
			Gdx.app.exit();
		}
	}
	
	/**
	 * Iterates through given verts array and returns
	 * box2D compatible, meter-based coordinates for each.
	 * @param verts array of pixel-based verticle coordinates
	 * @return array of converted coordinates to metric system
	 */
	private float[] getMetricVerts( float[] verts ) {
		float[] metricVerts = new float[ verts.length ];
		for( int i = 1; i < verts.length; i += 2 ) {
			float[] metricValues = physics.getMetersFromPixels( verts [i-1], verts[i] );
			metricVerts[i-1] = metricValues[0];
			metricVerts[i] = metricValues[1];
		}
		return metricVerts;
	}
	
	/**
	 * Creates fixture for PolyEntity
	 * @param originX orgin point for fixture (in pixels)
	 * @param originY orgin point for fixture (in pixels)
	 * @param verts fixture's shape verticies (mind the order)
	 */
	protected void setFixture ( float originX, float originY, float[] verts, BodyDef bodyDef ) {
		float[] pos = physics.getMetersFromPixels( originX, originY );
		
		verts = getMetricVerts( verts );
		
		bodyDef.position.set( new Vector2( pos[0], pos[1] ) );
		
		Body body = physics.world.createBody( bodyDef );
		
		PolygonShape shape = new PolygonShape();
		shape.set( verts );
		
		fixture = body.createFixture( shape, 1.0f );
		shape.dispose();
	}
	
	/**
	 * This method updates PolygonSprite transformations to match Fixture.
	 */
	@Override
	public void update () {
		float pos[] = physics.getPixelsFromMeters(
				fixture.getBody().getPosition().x,
				fixture.getBody().getPosition().y
			);
		poly.setPosition( pos[0], pos[1] );
	}
	
	/**
	 * This method executes drawing instruction on given batch
	 * @param batch PolygonSpriteBatch on which to draw 
	 * (needs to be enclosed with begin() and end()!)
	 */
	@Override
	public void render ( PolygonSpriteBatch polyBatch ) {
		poly.draw( polyBatch );
	}
}
