package com.elchupacabra.elchupapatrol.entities;


/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Use this class to store all important data that need to be
 * extracted from fixtures. (Eg. type of entity, for collision listener)
 *
 */
public class EntityUserData {
	
	/** pawn types */
	public static enum PawnType {
		WHEEL, CHASSIS, TURRET, ENEMY_FLYER, 
		BULLET, MINE, STONE, GROUND_SLICE
	};
	
	/** selected pawn type. This type will be read by collision listener
	 * so it can decide where to redirect collision event info, based on
	 * what have collided with what. */
	public PawnType type;
	public PawnType collidedType;
	
	public boolean isNonColliding = false;
	public boolean bulletCollision = false;
	
	public EntityUserData ( PawnType type ) {
		this.type = type;
	}
}
