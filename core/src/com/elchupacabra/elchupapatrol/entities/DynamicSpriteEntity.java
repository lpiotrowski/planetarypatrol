package com.elchupacabra.elchupapatrol.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * 
 * @author Łukasz Piotrowski
 * 
 * Dynamic Entity allows to create sprites with dynamic fixtures.
 *
 */
public abstract class DynamicSpriteEntity extends SpriteEntity {
	/** specifies shape type: 'circle' or 'box' */
	private String shapeType;

	/**
	 * Creates DynamicEntity with specified shape type and dimensions
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param width width of Entity (in pixels)
	 * @param height height of Entity (in pixels)
	 * @param shapeType "circle" or "box". Defines shape for physics engine.
	 */
	public DynamicSpriteEntity ( float x, float y, float width, float height, String shapeType ) {
		super();
		this.shapeType = shapeType;
		setFixture ( x, y, width, height );
		setSprite( x, y, width, height );
	}
	
	/**
	 * Creates DynamicEntity with specified shape type and dimensions
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param size size of Entity (in pixels)
	 * @param shapeType "circle" or "box". Defines shape for physics engine.
	 */
	public DynamicSpriteEntity ( float x, float y, float size, String shapeType ) {
		super();
		this.shapeType = shapeType;
		setFixture ( x, y, size, size );
		setSprite( x, y, size, size );
	}
	
	/**
	 * Creates fixture with Dynamic Body Type.
	 * @param x position in world (in pixels)
	 * @param y position in world (in pixels)
	 * @param width width of Fixture (in pixels)
	 * @param height height of Fixture (in pixels)
	 */
	protected void setFixture ( float x, float y, float width, float height ) {
		float[] pos = physics.getMetersFromPixels( x, y );
		float[] size = physics.getMetersFromPixels( width, height );
		
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set( new Vector2( pos[0], pos[1] ) );
		
		Body body = physics.world.createBody( bodyDef );
		
		FixtureDef fixtureDef = new FixtureDef();
		Settings settings = Settings.getInstance();
		fixtureDef.density = (Float)settings.getValue( "entityDefaultDensity" );
		fixtureDef.friction = (Float)settings.getValue( "entityDefaultFriction" );
		fixtureDef.restitution = (Float)settings.getValue( "entityDefaultRestitution" );
		PolygonShape box = null;
		CircleShape circle = null;
		
		if( shapeType == "box" ){
			box = new PolygonShape();
			box.setAsBox( size[0], size[1] );
			fixtureDef.shape = box;
			fixture = body.createFixture( fixtureDef );
			box.dispose();
		} else {
			circle = new CircleShape();
			circle.setRadius( size[0] );
			fixtureDef.shape = circle;
			fixture = body.createFixture( fixtureDef );
			circle.dispose();
		}
	}
	
	/**
	 * Sets Fixture density
	 * @param value density value
	 */
	public void setDensity( float value ){
		fixture.setDensity( value );
	}
	
	/**
	 * Sets Fixture friction
	 * @param value friction value
	 */
	public void setFriction( float value ){
		fixture.setFriction( value );
	}
	
	/**
	 * Sets Fixture restitution
	 * @param value restitution value
	 */
	public void setRestitution( float value ){
		fixture.setRestitution( value );
	}
	
	

}