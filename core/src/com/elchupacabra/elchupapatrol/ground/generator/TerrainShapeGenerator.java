package com.elchupacabra.elchupapatrol.ground.generator;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * 
 * @author Łukasz Piotrowski
 *
 * Randomizes and modifies given vertices vertically
 * to create ridges, craters and hills
 */
public class TerrainShapeGenerator {
	/** random instance with time seed for randomizing output */
	private Random random;
	/** used as time parameter for sine functions */
	private double shapeFunctionTime;
	/** defines craterDepth at current verticle during crater building */
	private float craterDepth;
	/** times crater building phases */
	private CraterTimer craterTimer;
	
	public TerrainShapeGenerator () {
		random = new Random( System.currentTimeMillis() );
		craterTimer = new CraterTimer();
	}
	
	/**
	 * Create crater,
	 * @param vertexX x position of vertex to be modified
	 * @return modified vertex x coordinate
	 */
	private float createCraters ( float vertexX ) {
		if( craterTimer.isItTimeToBuildCrater() ){
			float x = (float) ( Math.PI * (float)craterTimer.timer / 
					(float)craterTimer.timerLimit );
			float depth = Settings.getInstance().getFloatValue( "craterDepth" );
			craterDepth = (float) ( Math.sin( x + Math.PI ) * depth );
			vertexX += craterDepth;
		}
		return vertexX;
	}
	
	/**
	 * Create random ridges
	 * @param vertexX x position of vertex to be modified
	 * @return modified vertex x coordinate
	 */
	private float createRidges ( float vertexX ) {
		float roughness = Settings.getInstance().getFloatValue( "roughness" );
		return vertexX + ( random.nextFloat() - 0.5f ) * roughness;
	}
	
	/**
	 * Create random sine-based hills
	 * @param vertexX x position of vertex to be modified
	 * @return  modified vertex x coordinate
	 */
	private float createHills ( float vertexX ) {
		Float[] stretch = Settings.getInstance().getFloatArrayValue( "hillsStretch" );
		float height = Settings.getInstance().getFloatValue( "hillsHeight" );
		shapeFunctionTime += 1;
		return vertexX + 
			( (float)Math.sin( shapeFunctionTime / stretch[0] ) +
			(float)Math.cos( shapeFunctionTime / stretch[1] ) -
			(float)Math.sin( shapeFunctionTime / stretch[2] ) )
			* height;
	}
	
	/**
	 * Apply all modifications for given vertex value
	 * (in order: hills->craters->ridges)
	 * @param vertexX x position of vertex to be modified
	 * @return  modified vertex x coordinate
	 */
	public float modifyTerrainVertex ( float vertexX ){
		vertexX = createHills( vertexX );
		vertexX = createCraters( vertexX );
		vertexX = createRidges( vertexX );
		return vertexX;
	}
	
	public void update () {
	}
	
}
