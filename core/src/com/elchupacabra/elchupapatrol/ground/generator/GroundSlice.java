package com.elchupacabra.elchupapatrol.ground.generator;

import com.badlogic.gdx.math.Vector2;
import com.elchupacabra.elchupapatrol.AutoRenderer;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.entities.KinematicPolyEntity;
import com.elchupacabra.elchupapatrol.entities.PolyEntity;
import com.elchupacabra.elchupapatrol.utils.Settings;

/**
 * 
 * @author Łukasz Piotrowski
 *
 * Single ground slice for ground generation.
 * 
 */

public class GroundSlice extends KinematicPolyEntity {
	
	protected float textureOffsetX = 0.0f;

	/**
	 * Creates ground slice
	 * @param originX point in world (in pixels)
	 * @param originY point in world (in pixels)
	 * @param verts array of verticies for polygon (mind the order)
	 * @param textureName name of texture from AssetsManager
	 */
	protected GroundSlice( float originX, float originY, float[] verts,
			String textureName, int textureOriginX ) {
		super( originX, originY, verts, textureName, textureOriginX, 0 );
		textureOffsetX = textureOriginX; 
		this.setFixtureUserData( new EntityUserData( EntityUserData.PawnType.GROUND_SLICE ) );
	}
	
	/**
	 * Allows to apply linear velocity to slice
	 * @param velocity value of linear velocity to apply horizontally
	 */
	public void moveSlice( float velocity ) {
		Settings settings = Settings.getInstance();
		fixture.getBody().setLinearVelocity(-settings.getFloatValue( "movementBaseSpeed" ), 0);
	}
	
	/**
	 * Returns slice position in world in pixels
	 * @return slice position
	 */
	public Vector2 getPosition() {
		float meterModifier = Settings.getInstance().getFloatValue( "pixelToMeterModifier" );
		Vector2 pos = fixture.getBody().getPosition();
		pos.scl( meterModifier );
		return pos;
	}

}
