package com.elchupacabra.elchupapatrol;

import com.badlogic.gdx.ApplicationAdapter;
/**
*	@author Łukasz Piotrowski
*
*	Main game class. Should stay unmodified
*	@todo: might be usefull though to create gui outside of the
*	game instance, so that may change
*/
public class ElcPatrolMain extends ApplicationAdapter {
	private GameCore core;

	
	/**
	*	Create method for creating new game
	*	instance
	*/
	@Override
	public void create () {
		this.core = new GameCore();
	}

	/**
	*	Main render method
	*	Do not modify.
	*	Runs both render and update from GameCore instance
	*/
	@Override
	public void render () {
		this.core.update();
		this.core.render();
	}
}
