package com.elchupacabra.elchupapatrol;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.elchupacabra.elchupapatrol.entities.Entity;
import com.elchupacabra.elchupapatrol.pawns.car.Car;

public class AutoRenderer {	
	private List<Entity> updateQueue;
	private List<ArrayList<Entity>> renderLayers;
	
	public static final int LAYERS_AMOUNT = 9;
	public static final int GAME_LAYER = 5;

	private static AutoRenderer instance = null;
	
	private AutoRenderer() {
		updateQueue = new ArrayList<Entity>();
		createRenderLayers();
	}
	
	/**
	 * Creates render layer for future entity injections
	 */
	private void createRenderLayers(){
		renderLayers = new ArrayList<ArrayList<Entity>>();
		for( int i=0; i<AutoRenderer.LAYERS_AMOUNT; i++ ){
			renderLayers.add( new ArrayList<Entity>() );
		}
	}
	
	/**
	 * Injects Entity to both render and update queue
	 * @param entity : Entity to be injected
	 * @param layer : layer number 0 .. AutoRenderer.LAYERS_AMOUNT
	 */
	public void inject( Entity entity, int layer ){
		injectToUpdate( entity );
		injectToRender( entity, layer );
	}
	
	/**
	 * Inject Entity to update
	 * @param entity : Entity to be injected
	 */
	public void injectToUpdate( Entity entity ){
		updateQueue.add( entity );
	}
	
	/**
	 * Remove Entity from update
	 * @param entity : Entity to be removed
	 */
	public void removeFromUpdate( Entity entity ){
		updateQueue.remove( entity );
	}
	
	
	/**
	 * Removes Entity from render
	 * @param entity : Entity to be removed
	 * @todo: finish that method
	 */
	public void removeFromRender( Entity entity ){
		for( ArrayList<Entity> layer : renderLayers) {
			int index = layer.indexOf( entity );
			if( index >= 0 ){
				Gdx.app.log("Renderer", "removed object in index: "+ String.valueOf(index) );
				layer.remove(index);
			}
		}
	}
	
	/**
	 * Injects Entity to renderer at given layer
	 * (AutoRenderer.GAME_LAYER = 5 is the layer of the game
	 * all other numbers are backgrounds and foregrounds)
	 * @param entity : Entity to be injected
	 * @param layer : layer number 0 .. AutoRenderer.LAYERS_AMOUNT
	 * @param insertAtIndex : forces to insert entity at given index ( another layering functionality within layer )
	 */
	public void injectToRender( Entity entity, int layer, int insertAtIndex ){
		if( layer > AutoRenderer.LAYERS_AMOUNT )
			layer = AutoRenderer.LAYERS_AMOUNT;		
		renderLayers.get( layer ).add( insertAtIndex, entity );
	}
	
	/**
	 * Injects Entity to renderer at given layer
	 * (AutoRenderer.GAME_LAYER = 5 is the layer of the game
	 * all other numbers are backgrounds and foregrounds)
	 * @param entity : Entity to be injected
	 * @param layer : layer number 0 .. AutoRenderer.LAYERS_AMOUNT
	 */
	public void injectToRender( Entity entity, int layer ){
		int insertAtIndex = renderLayers.get( layer ).size();
		injectToRender( entity, layer, insertAtIndex );
	}
	
	/**
	 * Returns instance of class or creates new
	 * if instance is null
	 * @return this class's instance
	 */
	public static AutoRenderer getInstance() {
		if( instance == null ){
			instance = new AutoRenderer();
			Gdx.app.log("Settings", "created Settings module");
		}
		return instance;
	}
	
	/**
	 * Renderer update method, for aux tasks that should
	 * not be in render method.
	 */
	protected void update () {
		for( int i=0; i<updateQueue.size(); i++ ){
			updateQueue.get( i ).update();
		}
	}
	
	/**
	 * Main render method - all injected polys and sprites are rendered here
	 * @param spriteBatch
	 * @param polyBatch
	 */
	protected void render ( SpriteBatch spriteBatch, PolygonSpriteBatch polyBatch ) {
		for( ArrayList<Entity> list : renderLayers) {
			for( Entity entity : list ){
				polyBatch.setProjectionMatrix( Camera.getInstance().camera.combined );
				spriteBatch.setProjectionMatrix( Camera.getInstance().camera.combined );
				polyBatch.begin();
					entity.render(polyBatch);
				polyBatch.end();
				spriteBatch.begin();
					entity.render( spriteBatch );
				spriteBatch.end();
			}
		}
	}
}
