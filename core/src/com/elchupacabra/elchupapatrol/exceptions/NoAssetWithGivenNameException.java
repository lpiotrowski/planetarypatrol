package com.elchupacabra.elchupapatrol.exceptions;

public class NoAssetWithGivenNameException extends Exception {

	private static final long serialVersionUID = -6381575308914624237L;
	private String name;
	private String type;
	
	public NoAssetWithGivenNameException( String name, String type ){
		this.name = name;
		this.type = type;
	}
	
	public String getMessage(){
		return "No asset of type: "+this.type+", with given name: "+this.name;
	}
}
