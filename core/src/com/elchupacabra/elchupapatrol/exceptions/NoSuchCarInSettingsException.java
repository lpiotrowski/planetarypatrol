package com.elchupacabra.elchupapatrol.exceptions;

import com.elchupacabra.elchupapatrol.utils.Settings;

public class NoSuchCarInSettingsException extends Exception {
	
	private static final long serialVersionUID = -4293210590056320439L;
	private int index;
	
	public NoSuchCarInSettingsException( int index ){
		this.index = index;
	}
	
	public String getMessage(){
		return 	"Cannot find car settings under index: "+ this.index +".\n"+
				"it's possible, that config files are either misconfigured or broken.";
	}
	
	public boolean isItDefaultCar(){
		return Settings.getInstance().DEFAULT_CAR_INDEX == index;
	}
}