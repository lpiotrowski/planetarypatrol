package com.elchupacabra.elchupapatrol.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.elchupacabra.elchupapatrol.exceptions.WrongConfigParameterException;

public class ConfigFilesLoader {
	
	private boolean isLoadingCarSettings = false;
	private HashMap<String, Object> currentCarConfig = null;
	
	public ConfigFilesLoader () {
		Gdx.app.log("Loader", "starting config loader");
		this.loadAllExistingConfigs();
		Gdx.app.log("Loader", "finished config loader");
	}
	
	/**
	 * @todo: make android/ios/desktop/web file loading automated
	 */
	public void loadAllExistingConfigs () {
		//Android
//		FileHandle[] files = Gdx.files.internal("config").list();
		//Desktop
		FileHandle[] files = Gdx.files.local("../android/assets/config").list();
		for( int fileNumber=0; fileNumber<files.length; fileNumber++) {
			this.isLoadingCarSettings = false;
			FileHandle file = files[fileNumber];
			Gdx.app.log("Loader", "reading file: " + file.name());
			String[] contents = file.readString().split("\n");
			parseConfigContents( contents, file.name() );
		}
	}
	
	private void parseConfigContents ( String[] contents, String filename ){
		for( int lineNumber=0; lineNumber<contents.length; lineNumber++ ){
			String setting = contents[lineNumber];
			try{
				parseKeyValuePair(setting, lineNumber, filename);
			} catch ( WrongConfigParameterException ex ) {
				Gdx.app.error("Config File Load Exception: ", ex.getMessage() );
				continue;
			} catch( NumberFormatException ex ) {
				Gdx.app.error("Config File Load Exception: ", ex.getMessage() );
				Gdx.app.exit();
				break;
			}
		}
		if( currentCarConfig != null ){
			Settings.getInstance().carsSettings.add( currentCarConfig );
			currentCarConfig = null;
		}
	}
	
	private void parseKeyValuePair ( String setting, int lineNumber, 
			String filename ) throws WrongConfigParameterException, NumberFormatException {
		if( setting.trim().length() > 0 ){ //ignore whitespace lines
			if( setting.charAt(0) != '#' ){ //ignore comments
				if( setting.trim().equalsIgnoreCase("CAR") ){ //ignore CAR tags and set car flag
					currentCarConfig = new HashMap<String, Object>();
					this.isLoadingCarSettings = true;
				} else {
					String[] pair = setting.split("=");
					if( pair.length < 2 ){
						throw new WrongConfigParameterException( setting, lineNumber, filename );
					} else {
						if( this.isLoadingCarSettings ){
							fillSetting( pair[0], pair[1], this.currentCarConfig );
							
						} else {
							fillSetting( pair[0], pair[1], Settings.getInstance().baseSettings );
						}
					}
				}
			}
		}
	}
	
	private void fillSetting ( String key, String value, HashMap<String, Object> settingsMap ) throws NumberFormatException{
		//read as float
		if( value.matches("^-?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?$") ){
			settingsMap.put(key, new Float( value ));
			Gdx.app.log("Loader FLOAT", key + " : " + new Float( value ).toString() );
		//read as array of floats
		} else if( value.matches("(-?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?\\,*)*")){
			String[] stringValues = value.replaceAll("\\p{Z}","").split(",");
			Float[] floatValues = new Float[stringValues.length];
			for( int i=0; i<stringValues.length; i++ ){
				Float floatValue = floatValues[i];
				String stringValue = stringValues[i];
				floatValue = new Float( stringValue );
				floatValues[i] = floatValue;
				Gdx.app.log("Loader ARRAY", key + " : " + floatValue.toString() );
			}
			settingsMap.put(key, floatValues);
		// read as bool
		} else if( value.contentEquals("true") || value.contentEquals("false") ) {
			settingsMap.put(key, new Boolean( value ));
			Gdx.app.log("Loader BOOLEAN", key + " : " + new Boolean( value ).toString() );
		//read as string
		} else {
			settingsMap.put(key, value);
			Gdx.app.log("Loader STRING", key + " : " + value);
		}
	}
}
