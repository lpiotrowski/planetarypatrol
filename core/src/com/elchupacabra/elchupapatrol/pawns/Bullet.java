package com.elchupacabra.elchupapatrol.pawns;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.elchupacabra.elchupapatrol.entities.DynamicSpriteEntity;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Bullet extends DynamicSpriteEntity {

	public Bullet(float x, float y, Vector2 direction, float force, String textureName ) {
		super(x, y, 15.0f, 15.0f, "circle");
		this.getBody().applyForceToCenter( 
				new Vector2(direction.nor().x*force, -direction.nor().y*force), 
				true);
		this.setTexture( textureName, false);
		Filter filter = new Filter();
		filter.maskBits = 0x0000;
		this.getBody().getFixtureList().get(0).setFilterData( filter );
		this.rotateToFlightPath();
	}
	//http://www.aurelienribon.com/blog/2011/07/box2d-tutorial-collision-filtering/
	public boolean isOutOfScreen(){
		Settings settings = Settings.getInstance();
		Vector2 pos = this.getBody().getPosition().scl(settings.getFloatValue("pixelToMeterModifier"));
		float margin = 50.0f;
		return (pos.x < -margin || pos.x > settings.getFloatValue("resX") + margin) ||
				(pos.y < -margin || pos.y > settings.getFloatValue("resY") + margin);
	}
	
	private void rotateToFlightPath(){
		float angle = this.getBody().getLinearVelocity().angleRad();
		this.getBody().setTransform(this.getBody().getPosition(), angle);
	}
	
	
	
	@Override
	public void update(){
		super.update();
		EntityUserData data = (EntityUserData)this.getBody().getUserData();
		if( this.isOutOfScreen() ){
			this.destroy();
		}
		this.rotateToFlightPath();
	}

}
