package com.elchupacabra.elchupapatrol.pawns.car;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Car {
	
	protected Wheel[] wheels = null;
	@SuppressWarnings("unused")
	private Suspension[] suspension = null;
	private Turret turret = null;
	protected Chassis chassis = null;
	@SuppressWarnings("unused")
	private Stabiliser stabiliser = null;
	private CruiseControl cruiseControl = null;
	private Timer jumpTimer;
	private final float JUMP_LOCK_TIME = 0.2f;
	private static boolean isJumping = false;
	
	private Settings settings = null;
	//TODO: add gun
	
	public static enum Controls {
		LEFT, RIGHT, JUMP, SHOOT
	};
	
	protected Car ( Wheel[] wheels, Chassis chassis, Suspension[] suspension, Stabiliser stabiliser, Turret turret ){
		this.wheels = wheels;
		this.chassis = chassis;
		this.suspension = suspension;
		this.stabiliser = stabiliser;
		this.turret = turret;
		this.cruiseControl = new CruiseControl();
		settings = Settings.getInstance();
		jumpTimer = new Timer();
	}
	
	protected void control ( Controls controls ){
		float accell = Gdx.input.getAccelerometerY();
		if(Gdx.input.isKeyPressed(Input.Keys.A) || accell < -0.5 ){
			runMotor( settings.getFloatCarValue( "impulseLeft" ) );
		} else if(Gdx.input.isKeyPressed(Input.Keys.D) || accell > 0.5 ){
			runMotor( -settings.getFloatCarValue( "impulseRight" ) );
		}
		
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			rocketJump( settings.getFloatCarValue( "impulseJump" ) );
		}
	}
	
	private void rocketJump( float force ){
		boolean isOnGround = false;
		for( Wheel wheel : wheels ){
			EntityUserData data;
			Fixture fixture =  wheel.getBody().getFixtureList().first();
			if( fixture.getUserData() instanceof EntityUserData ){
				data = ( EntityUserData )fixture.getUserData();
				if( !data.isNonColliding && !Car.isJumping ){
					isOnGround = true;
					Car.isJumping = true;
					jumpTimer.scheduleTask( new Task(){
						@Override
						public void run() {
							Car.isJumping = false;
						}
					}, this.JUMP_LOCK_TIME );
					break;
				}
			}
		}
		if( isOnGround ){
			chassis.getBody().applyForceToCenter( new Vector2( 0, force ), true );
			isJumping = true;
		}
	}
	
	private void runMotor( float torque ){
		for( int i = 0; i<wheels.length; i++ ){
			wheels[i].getBody().applyTorque( torque , true );
		}
	}
	
	public void update (){
//		for( int i = 0; i<wheels.length; i++ ){
//			wheels[i].update();
//		}
//		chassis.update();
//		turret.update();
//		runMotor( -10.0f );
		cruiseControl.forceConstantSpeed( this );
		control( null );
	}
	
	@Deprecated
	public void render ( SpriteBatch batch ) {
//		chassis.render( batch );
//		turret.render( batch );
//		for( int i = 0; i<wheels.length; i++ ){
//			wheels[i].render(batch);
//		}
	}
	
	protected void destroy () {
		for( int i=0; i<wheels.length; i++ ){
			wheels[i].destroy();
			wheels[i] = null;

		}
		turret.destroy();
		turret = null;
		chassis.destroy();
		chassis = null;
	}
	
}
