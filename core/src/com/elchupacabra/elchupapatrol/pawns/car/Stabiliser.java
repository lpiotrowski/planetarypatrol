package com.elchupacabra.elchupapatrol.pawns.car;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.JointDef.JointType;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.elchupacabra.elchupapatrol.physics.Physics;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Stabiliser {
	
	public RevoluteJoint joint;
	public Body stabiliserBlock;

	public Stabiliser( Chassis chassis ){
		createStabiliser( chassis );
	}
	
	private void setupStabiliserBlock ( Chassis chassis ){
		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set( 0, 0 );
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		stabiliserBlock = Physics.getInstance().world.createBody( bodyDef );
		Physics.getInstance().world.createJoint( getDistanceJointDefinition( chassis ) );
	}
	
	private RevoluteJointDef getRevoluteJointDefinition ( Chassis chassis ){
		Settings settings = Settings.getInstance();
		RevoluteJointDef jointDef = new RevoluteJointDef();
		jointDef.type = JointType.RevoluteJoint;
		jointDef.initialize( chassis.getBody(), stabiliserBlock, chassis.getBody().getPosition() );
		jointDef.enableLimit = settings.getBoolCarValue( "stabiliserLimit" );
		jointDef.upperAngle = (float)Math.PI/settings.getFloatCarValue( "stabiliserLimitUp" );
		jointDef.lowerAngle = -(float)Math.PI/settings.getFloatCarValue( "stabiliserLimitDown" );
		
		return jointDef;
	}
	
	private DistanceJointDef getDistanceJointDefinition ( Chassis chassis ){
		DistanceJointDef jointDef = new DistanceJointDef();
		jointDef.initialize( chassis.getBody(), stabiliserBlock,
				stabiliserBlock.getPosition(), chassis.getBody().getPosition());
		jointDef.collideConnected = false;
		jointDef.frequencyHz = 0;
		jointDef.dampingRatio = 0;
		return jointDef;
	}
	
	private void createStabiliser( Chassis chassis ){
		setupStabiliserBlock( chassis );
		Physics.getInstance().world.createJoint( getRevoluteJointDefinition( chassis ) );
	}
}
