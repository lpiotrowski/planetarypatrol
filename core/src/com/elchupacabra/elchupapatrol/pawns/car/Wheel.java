package com.elchupacabra.elchupapatrol.pawns.car;

import com.elchupacabra.elchupapatrol.entities.DynamicSpriteEntity;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Wheel extends DynamicSpriteEntity {

	public Wheel( float x, float y ) {
		super( 	x, y,
				Settings.getInstance().getFloatCarValue( "wheelsSize" ),
				"circle" );
		this.setFriction( Settings.getInstance().getFloatCarValue( "wheelsFriction" ) );
		this.setRestitution( Settings.getInstance().getFloatCarValue( "wheelsRestitution" ) );
		this.setDensity( Settings.getInstance().getFloatCarValue( "wheelsDensity" ) );
		this.setTexture("wheel", false );
		this.setFixtureUserData( new EntityUserData( EntityUserData.PawnType.WHEEL ) );
	}

}
