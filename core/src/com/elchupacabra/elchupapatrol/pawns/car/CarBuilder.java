package com.elchupacabra.elchupapatrol.pawns.car;

import com.elchupacabra.elchupapatrol.utils.Settings;

public class CarBuilder {

	private Car car = null;
	
	private Settings settings = null;
	
	public CarBuilder () {
		settings = Settings.getInstance();
	}
	
	public Car assemblyCar () {
		Float[] wheelsPositions = settings.getFloatArrayCarValue( "wheelsPositions" );
		Chassis chassis = new Chassis();
		Wheel[] wheels = new Wheel[ wheelsPositions.length ];
		Suspension[] suspension = new Suspension[ wheelsPositions.length ];
		Stabiliser stabiliser = new Stabiliser( chassis );
		setupUndercarriage( wheels, chassis, suspension );
		setupBody( chassis );
		Turret turret = new Turret( chassis );
		
		return car = new Car( wheels, chassis, suspension, stabiliser, turret );
	}
	
	private void setupUndercarriage ( Wheel[] wheels, Chassis chassis, Suspension[] suspension ){
		Float[] wheelsPositions = settings.getFloatArrayCarValue( "wheelsPositions" );
		for( int i=0; i<wheelsPositions.length; i++ ){
			float x = settings.getFloatCarValue( "defaultX" ) + settings.getFloatCarValue( "chassisWidth" ) 
					* wheelsPositions[i] ;
			float y = settings.getFloatCarValue( "defaultY" ) + settings.getFloatCarValue( "chassisHeight" ) +
					settings.getFloatCarValue( "suspensionHeight" );
			wheels[i] = new Wheel( x, y );
			suspension[i] = new Suspension( chassis, wheels[i] );
		}
	}
	
	private void setupTurret ( Chassis chassis, Turret turret ){
		
	}
	
	private void setupBody ( Chassis chassis ){
		//TODO: body collision shape
	}
	
	public void disassemblyCar () {
		car.destroy();
	}
}
