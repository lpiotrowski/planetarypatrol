package com.elchupacabra.elchupapatrol.pawns.car;

import com.elchupacabra.elchupapatrol.entities.DynamicSpriteEntity;
import com.elchupacabra.elchupapatrol.entities.EntityUserData;
import com.elchupacabra.elchupapatrol.utils.Settings;

public class Chassis extends DynamicSpriteEntity {

	public Chassis() {
		super(  Settings.getInstance().getFloatCarValue( "defaultX" ),
				Settings.getInstance().getFloatCarValue( "defaultY" ),
				Settings.getInstance().getFloatCarValue( "chassisWidth" ),
				Settings.getInstance().getFloatCarValue( "chassisHeight" ), 
				"box" );
		this.setTexture("chassis", false );
		this.setDensity( Settings.getInstance().getFloatCarValue( "chassisDensity" ) );
		this.setFixtureUserData( new EntityUserData( EntityUserData.PawnType.CHASSIS ) );
	}

}
